;;; Copyright © 2023 James David Clarke <james@jamesdavidclarke.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.


(define-module (gnu packages linuxmint)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages cinnamon)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages package-management)
  #:use-module (guix git-download))


(define-public webapp-manager
  (package
    (name "webapp-manager")
    (version "1.3.2")
    (source (origin
          (method git-fetch)
          (uri (git-reference
                (url "https://github.com/linuxmint/webapp-manager")
                (commit version)))
          (file-name (string-append name "-" version "-checkout"))
          (sha256 (base32 "1jayc805m716mm0p0bnjisb8clvqar87j65p4pv7ayybqirvkhga"))))

    (build-system gnu-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (delete 'check)
         (add-after 'unpack 'prepare
           (lambda _
             (substitute* "usr/lib/webapp-manager/webapp-manager.py"
               (("common-licenses/GPL") "licenses/common/GPL/license.txt")
               (("__DEB_VERSION__") ,version))
             #t))
         (replace 'build (lambda _ (zero? (system* "make"))))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (copy-recursively "etc" (string-append out "/etc"))
               (copy-recursively "usr" (string-append out "/usr"))))))))
    (native-inputs `(("pkg-config" ,pkg-config)))
    (propagated-inputs
     `(("python-beautifulsoup4" ,python-beautifulsoup4)
       ("python-configobj" ,python-configobj)
       ("python-gobject" ,python-gobject)
       ("python-pillow" ,python-pillow)
       ("python-setproctitle" ,python-setproctitle)
       ("python-tldextract" ,python-tldextract)
       ("dconf" ,dconf)
       ("libxapp" ,libxapp)))
    (home-page "https://github.com/linuxmint/webapp-manager")
    (synopsis "Run websites as if they were apps")
    (description "WebApp Manager allows you to run websites as if they were apps.")
    (license license:gpl3+)))


